# Running the Ansible Playbook

This playbook is designed to configure a Linux machine with various tasks using Ansible. Follow the steps below to run the playbook:

## Getting Started

1. Clone this repository:

    ```bash
    git clone https://gitlab.com/kaizenrz/ansible-playbook.git
    ```

2. Navigate to the cloned repository.


3. Install dependencies listed in `requirements.txt`:
- before installing dependencies make sure you have [Python](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installation/) installed in your environment then do :

    ```bash
    pip install -r requirements.txt
    ```
    or 
        ```bash
    pip3 install -r requirements.txt
    ```

4. Set up the Molecule test environment, you can read about Molecule [here](https://ansible.readthedocs.io/projects/molecule/).
- make sure to have Docker running then execute this command :

    ```bash
    molecule create
    ```

## Usage

To execute the playbook on the container, use:

```bash
molecule converge
 ```

to remove the container:

```bash
molecule destroy
```
